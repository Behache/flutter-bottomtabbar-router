import 'package:flutter/material.dart';
import 'package:bottom_tab_bar_router/navigation/navigation_delegate.dart';
import 'package:bottom_tab_bar_router/navigation/navigation_route_parser.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final navigationDelegate = NavigationDelegate();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      title: 'Bottom Tab Bar Router Template',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routeInformationParser: NavigationRouteParser(),
      routerDelegate: navigationDelegate,
    );
  }
}