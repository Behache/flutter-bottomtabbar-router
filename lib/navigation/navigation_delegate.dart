import 'package:bottom_tab_bar_router/ui/widgets/salomon_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:bottom_tab_bar_router/navigation/navigation_path.dart';
import 'package:bottom_tab_bar_router/ui/screens/screen1.dart';
import 'package:bottom_tab_bar_router/ui/screens/screen1_vm.dart';
import 'package:bottom_tab_bar_router/ui/screens/screen2.dart';
import 'package:bottom_tab_bar_router/ui/screens/screen2_vm.dart';
import 'package:bottom_tab_bar_router/ui/screens/screen3.dart';
import 'package:bottom_tab_bar_router/ui/screens/screen3_vm.dart';
import 'package:bottom_tab_bar_router/ui/widgets/my_custom_navigationbar.dart';


class NavigationDelegate extends RouterDelegate<NavigationPath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<NavigationPath>
    implements Screen1Router, Screen2Router, Screen3Router, NavigatorBarRouter, SalomonRouter {

 int _bottomNavigationBarIndex = 0;

  @override
  Widget build(BuildContext context) {
    final List<Page<dynamic>> pagesList = [];

    if (_bottomNavigationBarIndex == 0) {
      const screen1 = Screen1();
      pagesList.add(const MaterialPage(child: screen1));
    } else if(_bottomNavigationBarIndex == 1){
      const screen2 = Screen2();
      pagesList.add(const MaterialPage(child: screen2));
    } else if(_bottomNavigationBarIndex == 2){
      const screen3 = Screen3();
      pagesList.add(const MaterialPage(child: screen3, ));
      
    }
    

    return 
    Scaffold(
      // Comment next line and uncomment the next one to acceed to SalomonBar
      bottomNavigationBar:  MyCustomNavigationBar(router: this,),
    //  bottomNavigationBar:  SalomonBar(router: this,),
      body: Navigator(
        pages: pagesList,
        onPopPage: (route, result) {
          if (route.didPop(result) == false) {
            return false;
          }
          return onBackButtonTouched(result);
        },
      ),
    );
  }

  onBackButtonTouched(dynamic result) {
    
    return true;
  }

  @override
  NavigationPath? get currentConfiguration => NavigationPath(index: _bottomNavigationBarIndex);

  @override
  GlobalKey<NavigatorState>? navigatorKey = GlobalKey<NavigatorState>();

  @override
  Future<void> setNewRoutePath(NavigationPath configuration) async {
    final index = configuration.index;

   
  }
  
  @override
  displayScreen(int bottomBarindex) {
    _bottomNavigationBarIndex = bottomBarindex;
    notifyListeners();
  }

  
}
