import 'package:flutter/material.dart';
import 'package:bottom_tab_bar_router/navigation/navigation_path.dart';

class NavigationRouteParser extends RouteInformationParser<NavigationPath> {
  @override
  Future<NavigationPath> parseRouteInformation(
      RouteInformation routeInformation) {
    final uri = Uri.parse(routeInformation.location ?? "/");

    final int? index;
    if (uri.pathSegments.length >= 2) {
      index = int.tryParse(uri.pathSegments[1]);
    } else {
      index = 0;
    }

    return Future.value(NavigationPath(index: index));
  }

  @override
  RouteInformation? restoreRouteInformation(NavigationPath configuration) {
    String location = "/";
    final date = configuration.index;
    if (date != null) {
      location = "${location}date/$date";
    }
    return RouteInformation(location: location);
  }
}
