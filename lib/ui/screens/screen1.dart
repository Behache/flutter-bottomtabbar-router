import 'package:flutter/material.dart';

abstract class IScreen1Viewmodel extends ChangeNotifier{}

class Screen1 extends StatelessWidget {
  const Screen1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title: const Text("Screen 1"),),body: const Center(child: Text("Ecran 1"),),);
  }
}