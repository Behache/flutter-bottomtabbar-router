import 'package:flutter/material.dart';


abstract class IScreen2Viewmodel extends ChangeNotifier{}

class Screen2 extends StatelessWidget {
  const Screen2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title: const Text("Screen 2"),),body: const Center(child: Text("Ecran 2"),),);
  }
}