import 'package:flutter/material.dart';

abstract class IScreen3Viewmodel extends ChangeNotifier{}

class Screen3 extends StatelessWidget {
  const Screen3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title: const Text("Screen 3"),),body: const Center(child: Text("Ecran 3"),),);
}
}