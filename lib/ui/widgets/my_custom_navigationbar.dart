import 'package:flutter/material.dart';

abstract class NavigatorBarRouter {
  displayScreen(int bottomBarindex);
}

class MyCustomNavigationBar extends StatelessWidget {
  const MyCustomNavigationBar({
    Key? key,
    required this.router,
  }) : super(key: key);

  final NavigatorBarRouter router;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          IconButton(
            onPressed: () => router.displayScreen(0),
            icon: const Icon(
              Icons.home,
              color: Colors.blue,
            ),
          ),
          IconButton(
            onPressed: () => router.displayScreen(1),
            icon: const Icon(
              Icons.chat,
              color: Colors.blue,
            ),
          ),
          IconButton(
            onPressed: () => router.displayScreen(2),
            icon: const Icon(
              Icons.person,
              color: Colors.blue,
            ),
          ),
        ],
      ),
    );
  }
}
