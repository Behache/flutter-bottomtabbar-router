import 'package:flutter/material.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';

abstract class SalomonRouter {
  displayScreen(int bottomBarindex);
}

class SalomonBar extends StatefulWidget {
  const SalomonBar({
    Key? key,
    required this.router, 
  }) : super(key: key);

  final SalomonRouter router;

  @override
  State<SalomonBar> createState() => _SalomonBarState();
}

class _SalomonBarState extends State<SalomonBar> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return SalomonBottomBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
          widget.router.displayScreen(index);
        },
        items: [
          SalomonBottomBarItem(
            icon: const Icon(Icons.calendar_month),
            title: const Text("Screen1"),
            selectedColor: Colors.blue,
          ),
          SalomonBottomBarItem(
            icon: const Icon(Icons.group),
            title: const Text("Screen2"),
            selectedColor: Colors.blue,
          ),
          SalomonBottomBarItem(
            icon: const Icon(Icons.settings),
            title: const Text("Screen3"),
            selectedColor: Colors.blue,
          ),
        ]);
  }
}
